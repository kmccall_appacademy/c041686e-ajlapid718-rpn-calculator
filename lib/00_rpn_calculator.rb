class RPNCalculator
  def initialize
    @array_of_integers = []
  end

  def push(num)
    @array_of_integers << num
  end

  def value
    @array_of_integers.last
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def times
    perform_operation(:*)
  end

  def divide
    perform_operation(:/)
  end

  def tokens(string)
    tokens = string.split
    tokens.map { |char| operation?(char) ? char.to_sym : char.to_i}
  end

  def evaluate(string)
    tokens = tokens(string)

    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
    end

    value
  end

  private

  def operation?(char)
    [:+, :-, :*, :/].include?(char.to_sym)
  end

  def perform_operation(op_symbol)
      raise "calculator is empty" if @array_of_integers.size < 2

      second_operand = @array_of_integers.pop
      first_operand = @array_of_integers.pop

      case op_symbol
      when :+
        @array_of_integers << first_operand + second_operand
      when :-
        @array_of_integers << first_operand - second_operand
      when :*
        @array_of_integers << first_operand * second_operand
      when :/
        @array_of_integers << first_operand.fdiv(second_operand)
      else
        @array_of_integers << first_operand
        @array_of_integers << second_operand
        raise "No such operation: #{op_symbol}"
      end
    end
  end
